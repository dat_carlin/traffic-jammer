﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CameraController : MonoBehaviour
{
    [BoxGroup("References"), SerializeField] private Camera m_mainCamera;
    [BoxGroup("References"), SerializeField] private Transform target;
    [BoxGroup("References"), SerializeField] private Vector3 offset;

    private float smoothSpeed = 0.1f;

    private void Awake()
    {
        m_mainCamera = GetComponent<Camera>();
    }

    public void ChangeFov(float charLong, float speed)
    {
        smoothSpeed = speed;

        m_mainCamera.fieldOfView = 58f + (charLong * 1.5f);

        offset = new Vector3(offset.x, 1.5f + charLong / 10f, offset.z);
    }

    void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;

        //transform.LookAt(target);
    }
}
