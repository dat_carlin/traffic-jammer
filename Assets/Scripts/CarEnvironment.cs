﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CarEnvironment : MonoBehaviour
{

    [BoxGroup("References")] public Transform m_backConnector;

    public int m_indexInRow;

    private BoxCollider m_selfBoxCollider;

    private bool m_enableToMoveNextPosition;

    private float m_movementSpeed;

    private Vector3 m_currentPosition;
    private Vector3 m_nextPosition;


    private void Awake()
    {
        m_selfBoxCollider = GetComponent<BoxCollider>();
    }

   

    public void EnableToMoveNextPosition(float movementSpeed)
    {
        m_movementSpeed = movementSpeed;
        m_enableToMoveNextPosition = true;
        m_currentPosition = transform.position;
        m_nextPosition = m_currentPosition + new Vector3(0, 0, 4f);
    }

    private void DisableMoveNextPosition()
    {
        m_enableToMoveNextPosition = false;
        transform.position = m_nextPosition;
    }

    private void Update()
    {
        MoveToNextPosition();
    }

    private void MoveToNextPosition()
    {
        if (m_enableToMoveNextPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_nextPosition, Time.deltaTime * m_movementSpeed);

            if (Vector3.Distance(transform.position, m_nextPosition) < 0.1f)
            {
                DisableMoveNextPosition();
            }
        }
    }
}
