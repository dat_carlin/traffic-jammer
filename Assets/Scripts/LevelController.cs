﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    [BoxGroup("References")] public List<LineController> m_lineControllers = new List<LineController>();

    [BoxGroup("Buttons"), SerializeField] private Button m_changeSceneButton;
    //[BoxGroup("Buttons"), SerializeField] private Button m_nextLevelButton;
    //[BoxGroup("Buttons"), SerializeField] private Button m_tryAgainLevelButton;
    //[BoxGroup("Buttons"), SerializeField] private Button m_switchSkyboxButton;
    //[Space]
    //[BoxGroup("Progress"), SerializeField] private Text m_coinsText;
    //[BoxGroup("Progress"), SerializeField] private Text m_currentLevelText;
    //[BoxGroup("Progress"), SerializeField] private Text m_nextLevelText;
    //[Space]
    //[BoxGroup("Progress"), SerializeField] private Slider m_progressBar;
    //[Space]

    private int m_coinsCount;

    private bool m_levelWasFinished;


    private void Awake()
    {
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = -1;


        m_changeSceneButton.onClick.AddListener(RestartLevel);
        //m_nextLevelButton.onClick.AddListener(RestartLevel);
        //m_tryAgainLevelButton.onClick.AddListener(RestartLevel);

    }

    private void Start()
    {
        //PlayerPrefs.DeleteAll();
        PrepareCurrentLevel();
        DisableToMoveLines();
    }

    private void Update()
    {
        RotateSkybox();
    }

    private void LateUpdate()
    {
        //if (!m_playerController.m_isLevelFinished)
        //{
        //    CalculateProgressBarValue();
        //}
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void RotateSkybox()
    {
        if (RenderSettings.skybox != null)
        {
            RenderSettings.skybox.SetFloat("_Rotation", Time.time * 0.4f);
        }
    }


    public void UpdateCoinsCount()
    {
        m_coinsCount++;
        UpdateCoinsCountText();
    }

    private void UpdateCoinsCountText()
    {
        //m_coinsText.text = m_coinsCount.ToString();
    }



    public void FinishLevel()
    {
        if (!m_levelWasFinished)
        {
            //m_playerController.FixateFinishLevel(true);
            SaveNewLevel();
            m_levelWasFinished = true;
        }
    }

    private void SaveNewLevel()
    {
        PlayerPrefs.SetInt("CurrentLevelNum", PlayerPrefs.GetInt("CurrentLevelNum") + 1);
    }

    private void PrepareCurrentLevel()
    {
        if (PlayerPrefs.HasKey("CurrentLevelNum"))
        {
            SetLevelsText(PlayerPrefs.GetInt("CurrentLevelNum"));
        }
        else
        {
            SetLevelsText(1);
            PlayerPrefs.SetInt("CurrentLevelNum", 1);
        }
    }

    private void SetLevelsText(int currentLevel)
    {
        //m_currentLevelText.text = currentLevel.ToString();
        //m_nextLevelText.text = (currentLevel + 1).ToString();
    }

    public void DisableToMoveLines(LineController exceptionLine)
    {
        for (int i = 0; i < m_lineControllers.Count; i++)
        {
            m_lineControllers[i].m_mainCarIsInRow = false;
        }

        exceptionLine.m_mainCarIsInRow = true;
    }

    public void DisableToMoveLines()
    {
        for (int i = 0; i < m_lineControllers.Count; i++)
        {
            m_lineControllers[i].m_mainCarIsInRow = false;
        }
    }
}
