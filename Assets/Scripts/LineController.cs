﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class LineController : MonoBehaviour
{
    public enum LineOpenState
    {
        Closed,
        Opened
    }


    [BoxGroup("Settings")] public int m_lineIndex;

    [BoxGroup("References")] public List<CarEnvironment> m_carsInRowList = new List<CarEnvironment>();

    [HideInInspector] public int m_minMoveIndex = 1; //с этой позиции будет двигаться вся линия

    [HideInInspector] public bool m_enableToAddCar;
    [HideInInspector] public bool m_enableToMoveLine;
    [HideInInspector] public LineOpenState m_openState;
    [HideInInspector] public bool m_mainCarIsInRow;

    private LevelController m_levelController;

    private void Awake()
    {
        m_levelController = GameObject.FindGameObjectWithTag("LevelController").GetComponent<LevelController>();

    }

    private void Start()
    {
        RefreshList();
    }

    private void ChaneOpenState(LineOpenState state)
    {
        m_openState = state;
    }

    private void MoveUpCarsInRow(int startIndex)
    {
        if ((m_enableToMoveLine) && (m_openState == LineOpenState.Opened))
        {
            for (int i = startIndex; i < m_carsInRowList.Count; i++)
            {
                m_carsInRowList[i].EnableToMoveNextPosition(6f);
            }
        }

    }

    private void MoveDownCarsInROw(int startIndex)
    {
        if ((m_enableToMoveLine) && (m_openState == LineOpenState.Opened))
        {
            for (int i = 0; i < startIndex; i++)
            {
                if (MainCar.Instance.m_movementState == MainCar.MovementState.Moving) return;

                m_carsInRowList[i].EnableToMoveNextPosition(6f);
            }
        }

    }

    public void StartCarReception()
    {
        m_carsInRowList.Insert(m_minMoveIndex, MainCar.Instance.m_carEnvironment);

        RefreshList();

        m_levelController.DisableToMoveLines(this);

        DisableToMoveLine();
    }

    public void RemoveCarFromList(CarEnvironment item)
    {
        m_carsInRowList.Remove(item);
    }

    private IEnumerator EnableToMoveCarsInRow()
    {
        if (!m_mainCarIsInRow)
        {
            yield return new WaitForSeconds(0.7f);

            MoveUpCarsInRow(m_minMoveIndex);

            yield return new WaitForSeconds(1f);
            MoveDownCarsInROw(m_minMoveIndex);

            yield return new WaitForSeconds(0.5f);
            DisableToMoveLine();

            CheckNullPositionCars();
            CheckMaxPositionCars();

            yield return new WaitForSeconds(1f);

            EnableToMoveLine();
        }
    }

    public void RefreshList()
    {
        for (int i = 0; i < m_carsInRowList.Count; i++)
        {
            m_carsInRowList[i].m_indexInRow = i;
        }
    }

    public void EnableToMoveLine()
    {
        m_enableToMoveLine = true;
        StartCoroutine(EnableToMoveCarsInRow());
        ChaneOpenState(LineOpenState.Opened);
    }

    private void DisableToMoveLine()
    {
        m_enableToMoveLine = false;
        ChaneOpenState(LineOpenState.Closed);
    }

    private void CheckNullPositionCars()
    {
        for (int i = 0; i < m_carsInRowList.Count; i++)
        {
            if (m_carsInRowList[i].transform.position.z < 3f)
            {
                UnusedCarsPool.Instance.GetCarFromPool(this);
                break;
            }
        }
    }

    private void CheckMaxPositionCars()
    {
        List<CarEnvironment> carsForRemove = new List<CarEnvironment>();
        for (int i = 0; i < m_carsInRowList.Count; i++)
        {
            if (m_carsInRowList[i].transform.position.z >= 18f)
            {
                UnusedCarsPool.Instance.AddCarToPool(m_carsInRowList[i]);
                carsForRemove.Add(m_carsInRowList[i]);
            }
        }

        if (carsForRemove.Count > 0)
        {
            for (int i = 0; i < carsForRemove.Count; i++)
            {
                m_carsInRowList.Remove(carsForRemove[i]);
            }
        }
    }
}
