﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCar : MonoBehaviour
{

    public static MainCar Instance;

    public enum MovementState
    {
        StandStill,
        Moving
    }

    [HideInInspector] public MovementState m_movementState;

    private LevelController m_levelController;
    private int m_currentRowIndex;

    [HideInInspector] public int m_indexInRow;
    [HideInInspector] public CarEnvironment m_carEnvironment;

    private LineController m_lineController;
    private bool m_allowToChangePosition;

    private Vector3 m_newPosition;

    private float m_movementSpeed = 7f;

    private int m_indexOfNextLine;
    private int m_indexOfCurrentLine;

    private Animator m_selfAnimator;
    private string m_movementLeftAnimation = "MakeMovementLeft";
    private string m_movementRightAnimation = "MakeMovementRight";

    private void Awake()
    {
        m_levelController = GameObject.FindGameObjectWithTag("LevelController").GetComponent<LevelController>();
        m_selfAnimator = GetComponent<Animator>();
        m_carEnvironment = GetComponent<CarEnvironment>();
        Instance = this;
    }

    private void Start()
    {
        ChangeCurrentLineIndex(1); //starts always in second line
        ChangeMovementState(MovementState.StandStill);
        EnableToMoveSideLines();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ChooseLineAndMakeMove();
        }

        MoveCarToNewPosition();
    }


    private void ChangeMovementState(MovementState movementState)
    {
        m_movementState = movementState;
    }

    private void PlayAnimation(string animName)
    {
        m_selfAnimator.enabled = true;
        m_selfAnimator.Play(animName);
    }

    private void MoveCarToNewPosition()
    {
        if (m_allowToChangePosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_newPosition, Time.deltaTime * m_movementSpeed);

            if (Vector3.Distance(transform.position, m_newPosition) < 0.1f)
            {
                FixateArrivingToNewPosition();
            }
        }
    }

    private void FixateArrivingToNewPosition()
    {
        transform.position = m_newPosition;
        StartCoroutine(DisableMoveToNewPosition());

        transform.SetParent(m_levelController.m_lineControllers[m_indexOfNextLine].transform);

        EnableToMoveSideLines();

    }

    public void SetLineContoller(int indexOfLine)
    {
        m_lineController = m_levelController.m_lineControllers[indexOfLine];
    }

    private void SetIndexInRow()
    {
        m_indexInRow = m_lineController.m_carsInRowList.IndexOf(m_carEnvironment);
    }

    public void ChangeCurrentLineIndex(int index)
    {
        m_currentRowIndex = index;
    }

    private void EnableMoveToNewPosition()
    {
        m_allowToChangePosition = true;
        ChangeMovementState(MovementState.Moving);
    }

    private IEnumerator DisableMoveToNewPosition()
    {
        m_allowToChangePosition = false;
        yield return new WaitForSeconds(1f);
        ChangeMovementState(MovementState.StandStill);
    }

    private void ChooseLineAndMakeMove()
    {
        switch (m_currentRowIndex)
        {
            case 0: //can go to second line left
                {
                    TransterCarToNewRow(1, new Vector3(2f, 0f, 2.5f), m_movementRightAnimation);

                    break;
                }
            case 1: //can go to first and third line (random)
                {
                    int random = Random.Range(0, 3);

                    switch (random)
                    {
                        case 0:
                        case 1:
                            {
                                if (m_levelController.m_lineControllers[0].m_openState == LineController.LineOpenState.Opened)
                                {
                                    m_levelController.m_lineControllers[0].StartCarReception();
                                    m_levelController.m_lineControllers[m_currentRowIndex].RemoveCarFromList(m_carEnvironment);
                                    transform.SetParent(null);
                                    m_newPosition = transform.position + new Vector3(-2f, 0f, 2.5f);
                                    EnableMoveToNewPosition();
                                    m_indexOfNextLine = 0;
                                    PlayAnimation(m_movementLeftAnimation);
                                    ChangeCurrentLineIndex(0);
                                }
                                else
                                {
                                    if (m_levelController.m_lineControllers[2].m_openState == LineController.LineOpenState.Opened)
                                    {
                                        m_levelController.m_lineControllers[2].StartCarReception();
                                        m_levelController.m_lineControllers[m_currentRowIndex].RemoveCarFromList(m_carEnvironment);
                                        transform.SetParent(null);
                                        m_newPosition = transform.position + new Vector3(2f, 0f, 2.5f);
                                        EnableMoveToNewPosition();
                                        m_indexOfNextLine = 2;
                                        PlayAnimation(m_movementRightAnimation);
                                        ChangeCurrentLineIndex(2);
                                    }
                                }

                                break;
                            }
                        case 2:
                        case 3:
                            {
                                if (m_levelController.m_lineControllers[2].m_openState == LineController.LineOpenState.Opened)
                                {


                                    m_levelController.m_lineControllers[2].StartCarReception();
                                    m_levelController.m_lineControllers[m_currentRowIndex].RemoveCarFromList(m_carEnvironment);
                                    transform.SetParent(null);
                                    m_newPosition = transform.position + new Vector3(2f, 0f, 2.5f);
                                    EnableMoveToNewPosition();
                                    m_indexOfNextLine = 2;
                                    PlayAnimation(m_movementRightAnimation);
                                    ChangeCurrentLineIndex(2);
                                }
                                else
                                {
                                    if (m_levelController.m_lineControllers[0].m_openState == LineController.LineOpenState.Opened)
                                    {
                                        m_levelController.m_lineControllers[0].StartCarReception();
                                        m_levelController.m_lineControllers[m_currentRowIndex].RemoveCarFromList(m_carEnvironment);
                                        transform.SetParent(null);
                                        m_newPosition = transform.position + new Vector3(-2f, 0f, 2.5f);
                                        EnableMoveToNewPosition();
                                        m_indexOfNextLine = 0;
                                        PlayAnimation(m_movementLeftAnimation);
                                        ChangeCurrentLineIndex(0);
                                    }
                                }
                                break;
                            }
                    }
                    break;
                }
            case 2: //can go to secont line from right
                {
                    TransterCarToNewRow(1, new Vector3(-2f, 0f, 2.5f), m_movementLeftAnimation);

                    break;
                }
        }

    }

    private void TransterCarToNewRow(int nextLineIndex, Vector3 nextPositionOffset, string animationName)
    {
        if (m_levelController.m_lineControllers[nextLineIndex].m_openState == LineController.LineOpenState.Closed) return;

        m_levelController.m_lineControllers[nextLineIndex].StartCarReception();
        m_levelController.m_lineControllers[m_currentRowIndex].RemoveCarFromList(m_carEnvironment);
        transform.SetParent(null);
        m_newPosition = transform.position + nextPositionOffset;
        EnableMoveToNewPosition();
        m_indexOfNextLine = nextLineIndex;
        PlayAnimation(animationName);
        ChangeCurrentLineIndex(nextLineIndex);
    }

    private void EnableToMoveSideLines()
    {
        switch (m_currentRowIndex)
        {
            case 0:
            case 2:
                {
                    EnableToMoveLine(2);
                    break;
                }

            case 1:
                {
                    StartCoroutine(EnableToMoveLine(2f));
                    break;
                }
        }
    }

    private void EnableToMoveLine(int lineIndex)
    {
        m_levelController.m_lineControllers[lineIndex].EnableToMoveLine();
    }

    private IEnumerator EnableToMoveLine(float delay)
    {
        int random = Random.Range(0, 4);

        int firstLine = 0;
        int secondLine = 2;

        switch (random)
        {
            case 0:
            case 1:
                {
                    firstLine = 0;
                    secondLine = 2;
                    break;
                }
            case 2:
            case 3:
            case 4:
                {
                    firstLine = 2;
                    secondLine = 0;
                    break;
                }
        }


        m_levelController.m_lineControllers[firstLine].EnableToMoveLine();
        yield return new WaitForSeconds(delay);

        m_levelController.m_lineControllers[secondLine].EnableToMoveLine();

    }
}
