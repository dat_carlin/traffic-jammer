﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class UnusedCarsPool : MonoBehaviour
{
    public static UnusedCarsPool Instance;

    public List<CarEnvironment> m_carsPool = new List<CarEnvironment>();

    private void Awake()
    {
        Instance = this;
    }


    public void GetCarFromPool(LineController line)
    {
        if (m_carsPool.Count > 0)
        {
            CarEnvironment car = m_carsPool[0];

            car.transform.SetParent(line.transform);
            car.gameObject.SetActive(true);


            car.transform.SetSiblingIndex(0);
            line.m_carsInRowList.Insert(0, car);

            line.RefreshList();


            car.transform.position = line.m_carsInRowList[1].transform.position - new Vector3(0f, 0f, 8f);

            car.EnableToMoveNextPosition(8f);

            m_carsPool.Remove(car);
        }

        else
        {
            print("Add more cars to pool");
        }
    }

    public void AddCarToPool(CarEnvironment car)
    {
        m_carsPool.Add(car);
        car.gameObject.SetActive(false);
        car.transform.SetParent(transform);
    }

}
